﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Calc;

namespace CalcProject.Tests
{
    [TestClass]
    public class UnitTests
    {

        [TestMethod]
        public void ReturnsZeroForEmptyString()
        {
            // arrange
            Calculator calculator = new Calculator();
            string text = String.Empty;

            // act
            var result = calculator.Add(text);

            // assert
            Assert.AreEqual(0, result);
        }

        [TestMethod]
        public void ReturnsValueForStringWithOneValue()
        {
            // arrange
            Calculator calculator = new Calculator();
            string text = "5";

            // act
            var result = calculator.Add(text);

            // assert
            Assert.AreEqual(5, result);
        }

        [TestMethod]
        public void SumsCommaDelimitedValues()
        {
            // arrange
            Calculator calculator = new Calculator();
            string text = "2,5";

            // act
            var result = calculator.Add(text);

            // assert
            Assert.AreEqual(7, result);
        }

        [TestMethod]
        public void SumsMoreThanTwoNumbers()
        {
            // arrange
            Calculator calculator = new Calculator();
            string text1 = "2,5,5";
            string text2 = "3,0,10,7,10,1";

            // act
            var result1 = calculator.Add(text1);
            var result2 = calculator.Add(text2);

            // assert
            Assert.AreEqual(12, result1);
            Assert.AreEqual(31, result2);
        }

        [TestMethod]
        public void SumsNewlineAndCommaDelimitedValues()
        {
            // arrange
            Calculator calculator = new Calculator();
            string text1 = "2\n5";
            string text2 = "10,1\n10";

            // act
            var result1 = calculator.Add(text1);
            var result2 = calculator.Add(text2);

            // assert
            Assert.AreEqual(7, result1);
            Assert.AreEqual(21, result2);
        }

        [TestMethod]
        public void SumsValuesDelimitedByCustomCharDelimiter()
        {
            // arrange
            Calculator calculator = new Calculator();
            string text1 = "//a\n2a5";
            string text2 = "//;\n5;10;5";

            // act
            var result1 = calculator.Add(text1);
            var result2 = calculator.Add(text2);

            // assert
            Assert.AreEqual(7, result1);
            Assert.AreEqual(20, result2);
        }

        [TestMethod]
        [ExpectedException(typeof(NegativesNotAllowedException))]
        public void ThrowsNegativesNotAllowedException()
        {
            // arrange
            Calculator calculator = new Calculator();
            string text = "1,-2,-3,4";

            // act
            var result1 = calculator.Add(text);

            // assert
            // Exception is thrown
        }

        [TestMethod]
        public void IgnoresNumbersBiggerThan1000()
        {
            // arrange
            Calculator calculator = new Calculator();
            string text1 = "2,1001";
            string text2 = "1001,1001";

            // act
            var result1 = calculator.Add(text1);
            var result2 = calculator.Add(text2);

            // assert
            Assert.AreEqual(2, result1);
            Assert.AreEqual(0, result2);
        }


        [TestMethod]
        public void SumsValuesDelimitedByMulticharDelimiters()
        {
            // arrange
            Calculator calculator = new Calculator();
            string text1 = "//[***]\n1***2***3";
            string text2 = "//[a]\n2a10a1";

            // act
            var result1 = calculator.Add(text1);
            var result2 = calculator.Add(text2);

            // assert
            Assert.AreEqual(6, result1);
            Assert.AreEqual(13, result2);
        }

        [TestMethod]
        public void SumsValuesDelimitedByManyCharDelimiters()
        {
            // arrange
            Calculator calculator = new Calculator();
            string text1 = "//[*][a]\n1*2a3";
            string text2 = "//[a][x]\n2x1a1";

            // act
            var result1 = calculator.Add(text1);
            var result2 = calculator.Add(text2);

            // assert
            Assert.AreEqual(6, result1);
            Assert.AreEqual(4, result2);
        }

        [TestMethod]
        public void SumsValuesDelimitedByManyMulticharDelimiters()
        {
            // arrange
            Calculator calculator = new Calculator();
            string text1 = "//[**][a]\n1**2a3";
            string text2 = "//[aaa][xax]\n2xax1aaa1";

            // act
            var result1 = calculator.Add(text1);
            var result2 = calculator.Add(text2);

            // assert
            Assert.AreEqual(6, result1);
            Assert.AreEqual(4, result2);
        }
    }
}
