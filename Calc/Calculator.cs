﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace Calc
{

    public class Calculator
    {
        public int Add(string text)
        {
            if (text.Length == 0)
                return 0;

            string pattern;

            if (text.Length > 4 && text.Substring(0, 2) == "//")
            {
                pattern = GetDelimitersPattern(text);
                text = String.Join("", text.Skip(text.IndexOf('\n')+1).ToArray());
            }
            else
                pattern = "\n|,";


            var splitted = Regex.Split(text, pattern);
            var numbers = new List<int>();
            foreach(var token in splitted)
            {
                var number = Int32.Parse(token);

                if (number < 0)
                    throw new NegativesNotAllowedException();
                if (number > 1000)
                    continue;

                numbers.Add(number);
            }
            return numbers.Sum();
        }

        private string GetDelimitersPattern(string text)
        {
            var delimiters = new List<string>() { };
            var matches = Regex.Matches(text, @"\[[^\]]*\]");

            if (matches.Count > 0)
            {
                foreach (Match match in matches)
                {
                    var raw = match.Value.Substring(1, (match.Value.Length - 2));
                    delimiters.Add(Regex.Escape(raw));
                }     
            }
                
            else
                delimiters.Add(Char.ToString(text[2]));

           
            return "\n|,|" + String.Join("|", delimiters) + "";
        }
    }

    public class NegativesNotAllowedException : Exception
    {
        private static string _message = "Negatives not allowed!";
        public NegativesNotAllowedException()
        {
        }

        public NegativesNotAllowedException(string message) : base(message)
        {
        }

        public NegativesNotAllowedException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected NegativesNotAllowedException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
